# To apply these commands to all JPG and PNG files in a directory, run
#		exiv2 -ukv -n UTF-8 -m "blair-junction-logo-exiv2_commands.txt" blair-junction*.jpg blair-junction*.png blair-junction*.gif

# To modify a few metadata points with inline commands, run something like:
#		exiv2 -ukv -n UTF-8 -M"set Exif.Image.ImageDescription TITLE" image.jpg
#		exiv2 -ukv -n UTF-8 -M"set Xmp.dc.title TITLE" image.jpg

# Command file format
# -------------------
# Empty lines and lines starting with # are ignored
# Each remaining line is a command. The format for command lines is
# <cmd> <key> [[<type>] <value>]
# cmd = set|add|del
#    set will set the value of an existing tag of the given key or set a tag
#    add will add a tag (unless the key is a non-repeatable IPTC key)
#    del will delete a tag
# key = Exiv2 Exif or IPTC key
# type =
#    Byte|Ascii|Short|Long|Rational|Undefined|SShort|SLong|SRational|Comment
#       for Exif keys, and
#    String|Date|Time|Short|Undefined  for IPTC keys
#    The format for IPTC Date values is YYYY-MM-DD (year, month, day) and
#    for IPTC Time values it is HH:MM:SS±HH:MM where HH:MM:SS refers to local
#    hour, minute and seconds and ±HH:MM refers to hours and minutes ahead or
#    behind Universal Coordinated Time.
#    A default type is used if none is explicitely given. The default type
#    is determined based on the key.
# value
#    The remaining text on the line is the value. It can optionally be enclosed in
#    double quotes ("value")



# ==== Metadta Particular To the Individual Work ==== #

### the title
set Exif.Image.ImageDescription		Blair Junction logo
set Xmp.dc.title					Blair Junction logo

### publication date: YYYY-MM-DD
set Xmp.dc.date						2015-02-07


### comma-separated list of keywords
### try to cover all of Who, What, Where, How, When, Why;
### People, Places, Plants, Animals, Weather, Time of Day, Things, and Strong Effects (motion blur, and so on)
set Xmp.dc.subject					Nevada
set Xmp.dc.subject					maps
set Xmp.dc.subject					flags
set Xmp.dc.subject					old west
set Xmp.dc.subject					skulls
set Xmp.dc.subject					sage brush
set Xmp.dc.subject					murder mystery
set Xmp.dc.subject					web games


### software originally used to *create* the image:
#### application name and version number - e.g., App x.x
set Exif.Image.Software						Inkscape 0.48

#### application name
set Xmp.xmp.CreatorTool			XmpText		Inkscape
set Iptc.Application2.Program				Inkscape

#### application version number - e.g., x.x
set Iptc.Application2.ProgramVersion		0.48





# ==== Licensing Statements ==== #

set Xmp.dc.creator				XmpSeq		Andrew Toskin
set Xmp.xmpRights.Owner			XmpBag		Andrew Toskin
set Iptc.Application2.Byline				Andrew Toskin
set Exif.Image.Artist						Andrew Toskin

set Xmp.xmpRights.WebStatement	XmpText		http://creativecommons.org/licenses/by-sa/4.0/

set Iptc.Application2.Copyright				copyleft Andrew Toskin, 2014. This work is published under a Creative Commons Attribution-ShareAlike 4.0 International License. Meaning basically that you’re free to copy, share, and remix this work, for any purpose, as long as you credit me as the source and “share alike.” See http://creativecommons.org/licenses/by-sa/4.0/

set Xmp.xmpRights.UsageTerms	LangAlt		copyleft Andrew Toskin, 2014. This work is published under a Creative Commons Attribution-ShareAlike 4.0 International License. Meaning basically that you’re free to copy, share, and remix this work, for any purpose, as long as you credit me as the source and “share alike.” See http://creativecommons.org/licenses/by-sa/4.0/

set Exif.Image.Copyright					copyleft Andrew Toskin, 2014. This work is published under a Creative Commons Attribution-ShareAlike 4.0 International License. Meaning basically that you’re free to copy, share, and remix this work, for any purpose, as long as you credit me as the source and “share alike.” See http://creativecommons.org/licenses/by-sa/4.0/

set Xmp.dc.rights				LangAlt		copyleft Andrew Toskin, 2014. This work is published under a Creative Commons Attribution-ShareAlike 4.0 International License. Meaning basically that you’re free to copy, share, and remix this work, for any purpose, as long as you credit me as the source and “share alike.” See http://creativecommons.org/licenses/by-sa/4.0/





# ==== Metadata Common To All CureForNightmares.com Comics ==== #

set Xmp.dc.publisher			XmpBag		Cure For Nightmares

set Xmp.iptc.CiUrlWork			XmpText		http://curefornightmares.com

set Xmp.iptc.CiEmailWork		XmpText		andrew@tosk.in
set Iptc.Application2.Contact				Andrew Toskin <andrew@tosk.in>

set Xmp.dc.type					XmpBag		vector graphics

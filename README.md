# Blair Junction

An old-west murder mystery you can play in a modern web browser… one day.
This project is not yet really playable, as I keep starting over.

This game is completely free and open source.

Code uses the the Mozilla Public License, version 2.0. See `MPLv2.md`, or
[read it online](https://www.mozilla.org/MPL/2.0/).
[ChooseALicense.com has a pretty good summary](http://choosealicense.com/licenses/mpl-2.0/).

Multimedia assets are published under the Creative Commons
Attribution-ShareAlike 4.0 International license. See `CC-BY-SA.md`, or
read it on the
[Creative Commons website](https://creativecommons.org/licenses/by-sa/4.0/).


## Attributions

Some images in this game were based on work by others:

* [The Sheriff](http://www.flickr.com/photos/katsrcool/13341988523) --- by [Kool Cats Photography](http://www.flickr.com/photos/katsrcool/13341988523)
* [Flag-Map of Nevada](http://commons.wikimedia.org/wiki/File:Flag-map_of_Nevada.svg) --- by [Fry1989](http://commons.wikimedia.org/wiki/User:Fry1989) and [Darwinek](http://commons.wikimedia.org/wiki/User:Darwinek)
